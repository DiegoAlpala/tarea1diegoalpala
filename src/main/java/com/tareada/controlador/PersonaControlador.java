package com.tareada.controlador;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.tareada.excepcion.ModeloNotFoundxception;
import com.tareada.modelo.Persona;
import com.tareada.servicio.impl.PersonaServicioImpl;

@RestController
@RequestMapping("/personas")
public class PersonaControlador {
	
	@Autowired
	private PersonaServicioImpl servicio;
	
	@GetMapping
	public ResponseEntity<List<Persona>> listarPersonas(){
		return new ResponseEntity<List<Persona>>(servicio.listar(), HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Persona> listarPorId(@PathVariable Integer id){
		Persona persona = servicio.listarPorId(id);
		if(persona == null) {
			throw new ModeloNotFoundxception("ID NO ENCONTRADO " + id);
		}
		return new ResponseEntity<Persona>(persona, HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<Persona> registrar(@Valid @RequestBody Persona obj){
		
		servicio.registrar(obj);		
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getIdPersona()).toUri();
		return ResponseEntity.created(location).build();	
	}
	
	@PutMapping
	public ResponseEntity<Persona> modificar(@Valid @RequestBody Persona persona) {
		Persona obj = servicio.modificar(persona);
		return new ResponseEntity<Persona>(obj, HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Object> eliminar(@PathVariable("id") Integer id) {
		servicio.eliminar(id);
		return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
	}
	

}
