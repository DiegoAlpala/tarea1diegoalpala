package com.tareada;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Tarea1DiegoAlpalaApplication {

	public static void main(String[] args) {
		SpringApplication.run(Tarea1DiegoAlpalaApplication.class, args);
	}

}
