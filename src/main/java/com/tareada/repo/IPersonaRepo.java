package com.tareada.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tareada.modelo.Persona;

public interface IPersonaRepo extends JpaRepository<Persona, Integer> {

}
