package com.tareada.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.tareada.modelo.DetalleVenta;

public interface IDetalleVentaRepo extends JpaRepository<DetalleVenta, Integer> {

	@Modifying
	@Query(value = "INSERT INTO detalle_consulta(id_venta, id_producto, cantidad) values(:idVenta, :idProducto, :cantidad)", nativeQuery = true)
	Integer registrar(@Param("idVenta") int idVenta,@Param("idProducto") int idProducto, @Param("cantidad") int cantidad);
	
}
