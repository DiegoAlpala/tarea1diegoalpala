package com.tareada.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tareada.modelo.Venta;

public interface IVentaRepo extends JpaRepository<Venta, Integer> {

}
