package com.tareada.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tareada.modelo.Producto;

public interface IProductoRepo extends JpaRepository<Producto, Integer> {

}
