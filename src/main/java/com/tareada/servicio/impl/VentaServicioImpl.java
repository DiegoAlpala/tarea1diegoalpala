package com.tareada.servicio.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tareada.modelo.Venta;
import com.tareada.repo.IVentaRepo;
import com.tareada.servicio.IVentaServicio;

@Service
public class VentaServicioImpl implements IVentaServicio {

	@Autowired
	private IVentaRepo ventaRepo;
	
	@Override
	public Venta registrar(Venta obj) {
		
		obj.getDetalleVenta().forEach(dv -> {
			dv.setVenta(obj);
		});
		
		
		return ventaRepo.save(obj);
	}

	@Override
	public Venta modificar(Venta obj) {
		return ventaRepo.save(obj);
	}

	@Override
	public List<Venta> listar() {
		return ventaRepo.findAll();
	}

	@Override
	public Venta listarPorId(Integer id) {
		Optional<Venta> venta = ventaRepo.findById(id);
		return venta.isPresent()? venta.get() : null;
	}

	@Override
	public boolean eliminar(Integer id) {
		ventaRepo.deleteById(id);
		return false;
	}

}
