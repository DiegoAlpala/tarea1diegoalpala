package com.tareada.servicio.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tareada.modelo.Producto;
import com.tareada.repo.IProductoRepo;
import com.tareada.servicio.IProductoServicio;

@Service
public class ProductoServicioImpl implements IProductoServicio {

	@Autowired
	private IProductoRepo productoRepo;
	
	@Override
	public Producto registrar(Producto obj) {
		return productoRepo.save(obj);
	}

	@Override
	public Producto modificar(Producto obj) {
		return productoRepo.save(obj);
	}

	@Override
	public List<Producto> listar() {
		return productoRepo.findAll();
	}

	@Override
	public Producto listarPorId(Integer id) {
		Optional<Producto> producto = productoRepo.findById(id);
		return producto.isPresent()? producto.get() : null;
	}

	@Override
	public boolean eliminar(Integer id) {
		productoRepo.deleteById(id);
		return false;
	}

}
