package com.tareada.servicio.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tareada.modelo.Persona;
import com.tareada.repo.IPersonaRepo;
import com.tareada.servicio.IPersonaServicio;

@Service
public class PersonaServicioImpl implements IPersonaServicio {

	@Autowired
	private IPersonaRepo personaRepo;
	
	@Override
	public Persona registrar(Persona obj) {
		return personaRepo.save(obj);
	}

	@Override
	public Persona modificar(Persona obj) {
		return personaRepo.save(obj);
	}

	@Override
	public List<Persona> listar() {
		return personaRepo.findAll();
	}

	@Override
	public Persona listarPorId(Integer id) {
		Optional<Persona> persona = personaRepo.findById(id);
		return persona.isPresent()? persona.get() : null;
	}

	@Override
	public boolean eliminar(Integer id) {
		personaRepo.deleteById(id);
		return false;
	}

}
